import React from 'react';
import ReactDOM from 'react-dom';
import CommentDetail from './CommentDetail';
import faker from 'faker';
import ApprovalCard from './ApprovalCard';

const App=() => {
    return (
    <div className="ui container comments">
        <ApprovalCard>
            <CommentDetail author="shameem" date="Today at 6:00 PM" about="Nice blog post" avatar={faker.image.avatar()}/>
        </ApprovalCard>
        <ApprovalCard>
            <CommentDetail author="shafah" date="Yesterday at 6:00 PM" about="Excellent blog post" avatar={faker.image.avatar()}/>
        </ApprovalCard>
        <ApprovalCard>
            <CommentDetail author="shahla" date="Today at 4:00 PM" about="Good blog post" avatar={faker.image.avatar()}/>
        </ApprovalCard>
    </div>
    )
};

ReactDOM.render(<App />,document.querySelector('#root'))